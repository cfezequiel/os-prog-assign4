/**
 * COP 6611: Operating Systems (Spring 2012)
 * Programming Assignment 4: Sockets
 *
 * Author: Carlos Ezequiel
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>

#define BUFFER_SIZE         100
#define SHARED_BUFFER_SIZE  15
#define MAX_CONNECTIONS     5
#define SLEEP_TIME_SEC      2
#define DEFAULT_PORT        1050
#define N_CONNECT_ACCEPTS   3

/* 
 * Shared memory for an incoming message.
 * This stores only one message.
 */
char g_shared[SHARED_BUFFER_SIZE];

/*
 * Mutex for protecting the shared memory
 */
pthread_mutex_t lock;

/* 
 * Prints error then closes program 
 *
 * Note: This function was taken from server.c example in the
 * C Sockets Tutorial:
 * http://www.linuxhowtos.org/C_C++/socket.htm
 */
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

/**
 * Thread function to Handle client connection
 */
void *handle_client_connection(void *arg)
{
    char buffer[BUFFER_SIZE];
    int psockfd;
    int n;
    
    /* Convert input to proper format */
    psockfd = *((int *) arg);

    /* Initialize buffers */
    bzero(buffer, BUFFER_SIZE);

    /* Read message */
    n = recv(psockfd, buffer, BUFFER_SIZE, 0);
    if (n < 0) 
    {
        error("ERROR reading from socket");
    }

    /* Store the message in shared memory */
    pthread_mutex_lock(&lock);
    strncpy(g_shared, buffer, SHARED_BUFFER_SIZE);

    /* Print the message stored in shared memory*/ 
    printf("Shared memory: %s\n", g_shared);
    pthread_mutex_unlock(&lock);

    /* Go to sleep */
    sleep(SLEEP_TIME_SEC);

    /* Send contents of shared memory */
    pthread_mutex_lock(&lock);
    n = send(psockfd, g_shared, SHARED_BUFFER_SIZE, 0);
    pthread_mutex_unlock(&lock);
    if (n < 0) 
    {
        error("ERROR writing to socket");
    }

    /* Close the client connection */
    close(psockfd);

    return NULL;
}

/* 
 * Prints error then closes program 
 *
 * The general structure of this code was taken from the server.c 
 * example found in the C Sockets Tutorial:
 * http://www.linuxhowtos.org/C_C++/socket.htm
 */
int main(int argc, char *argv[])
{
    int sockfd, clientsockfd, portno;
    socklen_t client_address_len;
    struct sockaddr_in server_addr, client_addr;
    pthread_t threadID;
    pthread_t threads[N_CONNECT_ACCEPTS];
    int nconnections = 0;
    int i;
    int rc;

    /* Initialize shared buffer */
    bzero(g_shared, SHARED_BUFFER_SIZE);

    /* Check if port number is provided */
    if (argc < 2) 
    {
        /* If not, then set default port no */
        portno = DEFAULT_PORT;
    }
    else
    {
        /* Otherwise, convert given port no from string to integer */
        portno = atoi(argv[1]);
    }

    /* Create socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
    {
        error("ERROR opening socket");
    }

    /* Initialize server address */
    bzero((char *) &server_addr, sizeof(server_addr));


    /* Set server parameters */
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(portno);

    /* Bind socket to the address */
    if (bind(sockfd, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
    {
        error("ERROR on binding");
    }

    /* Listen for incoming connections */
    listen(sockfd, MAX_CONNECTIONS);
    client_address_len = sizeof(client_addr);

    /* Initialize mutex using defaults*/
    pthread_mutex_init(&lock, NULL);

    while (1)
    {
        /* Block until a client connects to server */
        clientsockfd = accept(sockfd, (struct sockaddr *) &client_addr,
                              &client_address_len);
        if (clientsockfd < 0) 
        {
            error("ERROR on accept");
        }

        /* Create thread to handle the connection*/
        rc = pthread_create(&threadID, NULL, handle_client_connection,
                       &clientsockfd);
        if (rc != 0)
        {
            printf("ERROR on thread creation");
        }
        
        /* Break when number of connection accepts have been reached */
        threads[nconnections++] = threadID;
#ifdef DEBUG
        printf("Number of connections: %d\n", nconnections);
#endif
        if (nconnections == N_CONNECT_ACCEPTS)
        {
            break;
        }
    }

    /* Wait on some 'loose threads' just in case */
    for (i = 0; i < nconnections; i++)
    {
        pthread_join(threads[i], NULL);
    }

    printf("Total connections: %d\n", nconnections);

    /* Close the socket */
    close(sockfd);

    printf("Done.\n");
    pthread_exit(NULL);

    return 0; 
}


