=========================================  

COP 6611: Operating Systems (Spring 2012)

Programming Assignment 4: Sockets

Author: Carlos Ezequiel

=========================================  

Compiling
---------

To compile the code, simply execute
$ make

To clean and compile code, run:
$ make clean server

Usage
------

$ ./server [port]

If [port] is not specified, then the server will use the default port 1050.
The server will run and accept a limited number of connections, afterwards it
would quit.

The default number of connection accepts is 3.